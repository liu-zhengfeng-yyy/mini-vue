import {getCurrentInstance, h, renderSlots, inject} from "../../lib/mini-vue.esm.js"

export const Foo = {
    name: 'Foo',
    setup(props, {emit}) {
        // 假设 props 有count
        const handleClick = (event) => {
            emit("click", event)
        }
        const injectData = inject('test');
        console.log(injectData);

        const instance = getCurrentInstance();
        console.log("Foo instance", instance)
        return {
            handleClick,
            injectData
        }
    },
    render() {
        const age = 18;
        console.log(this.$slots)
        return h('div', {
                onClick: this.handleClick
            },
            [
                renderSlots(this.$slots, 'header', {age}),
                h('div', {}, `count:${this.count}`),
                renderSlots(this.$slots, 'footer', {age}),
                h('div', {}, `injectData:${this.injectData}`)
            ]
        )
    }
}