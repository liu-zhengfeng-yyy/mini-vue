import {h, ref} from "../../lib/mini-vue.esm.js"

export const ArrayChildren = {
    name: 'ArrayChildren',
    render() {
        return h('div', {}, [
            h('button', {onClick: this.handleClick}, 'change'),
            h('div', {}, this.isChange ? [
                h('span', {key: 1}, '1'),
                h('span', {key: 2}, '2'),
                h('span', {key: 3}, '3'),
            ] : [
                h('span', {key: 1}, '1'),
                h('span', {key: 2}, '2'),
                h('span', {key: 3}, '3'),
                h('span', {key: 4}, '4'),
            ]),
        ])
    },
    setup() {
        const isChange = ref(true);

        function handleClick() {
            isChange.value = !isChange.value;
        }

        return {
            isChange,
            handleClick
        }
    }
}
