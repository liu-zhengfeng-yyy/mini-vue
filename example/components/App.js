import {createTextVNode, h, getCurrentInstance, provide, ref} from "../../lib/mini-vue.esm.js"
import {Foo} from './Foo.js'

export const App = {
    name: 'App',
    render() {
        return h(
            "div",
            {
                id: 'mini-vue',
                class: this.className,
            },
            [
                h(
                    'div',
                    {
                        class: this.className,
                        onClick: this.handleCountClick
                    },
                    'count:' + this.count),
                createTextVNode('纯文本节点'),
                h(Foo, {
                    count: 1,
                    onClick(event) {

                    }
                }, {
                    header: ({age}) => h('em', {}, 'header' + age),
                    footer: ({age}) => h('em', {}, 'footer' + age),
                })
            ]
        )
    },
    setup() {
        const instance = getCurrentInstance();
        console.log("App instance", instance);
        provide('test', 'test')
        const count = ref(1)

        function handleCountClick() {
            // count.value++
            // console.log(count.value)
            className.value = '234'
        }

        const className = ref('123')

        return {
            msg: 'mini-vue',
            count,
            handleCountClick,
            className
        }
    }
}