import {createRender} from '../runtime-core'
import {isOnStart} from "../shared/is";
import {VNode, VNodeEl} from "../runtime-core/type";

function createElement(type: string) {
    console.log('-----------调用createElement-----------')
    console.log('type:', type)
    return document.createElement(type)
}

function patchProps(el: HTMLElement, key: string, oldValue: any, newValue: any) {
    if (isOnStart(key)) {
        // 说明需要注册事件
        const eventName = key.slice(2).toLowerCase();
        el.addEventListener(eventName, newValue);
        return
    } else {
        if (newValue === null || newValue === undefined) {
            // 如果新值为空 则删除属性
            el.removeAttribute(key)
        } else {
            // 如果新值不为空 则添加属性
            el.setAttribute(key, newValue)
        }

    }
}

function insert(container: HTMLElement, element: HTMLElement, anchor: HTMLElement | null = null) {
    console.log('-------------insert------------------');
    console.log('container', container, 'element', element)
    // container.append(element)
    container.insertBefore(element, anchor)
}

function remove(el: HTMLElement) {
    const parentElement = el.parentElement;
    if (parentElement) {
        parentElement.removeChild(el)
    }
}

function setElementText(el: VNodeEl, text: string) {
    if (el instanceof HTMLElement) {
        el.textContent = text;
    }
}

/**
 * 浏览器环境的 render
 */
const renderer = createRender({createElement, patchProps, insert, remove, setElementText})


export function createApp(rootComponent: VNode) {
    return renderer.createApp(rootComponent)
}

// runtime-dom 依赖 runtime-core
export * from "../runtime-core";