import {ComponentInstance, NormalObject} from "./type";
import {hasOwn} from "../shared/object";

// 将所有特殊的属性和对应的映射 形成一个map进行管理
const publicPropertiesMap: NormalObject<Function> = {
    $el: (instance: ComponentInstance) => instance.vNode.el,
    $slots: (instance: ComponentInstance) => instance.slots,
}
export const publicInstanceProxyHandle = {
    get({_: instance}: { _: ComponentInstance }, key: string | symbol): any {
        const {setupState, props} = instance;
        // 获取当前组件中setup返回的数据 如果数据中 当前访问的参数有
        // 就返回当前的参数 进行代理
        if (hasOwn(setupState, key)) {
            return setupState?.[key];
        } else if (hasOwn(props, key)) {
            // 代理 props
            return props?.[key];
        }
        // 代理$el
        if (key in publicPropertiesMap) {
            return publicPropertiesMap[key](instance);
        }
    }
}
