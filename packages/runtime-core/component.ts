import {isFunction, isObject, isString} from "../shared/is";
import {ComponentInstance, VComponentNode, NormalObject, InstanceParent} from "./type";
import {publicInstanceProxyHandle} from "./componentPublicInstance";
import {initProps} from "./componentProps";
import {shallowReadonly} from "../reactive/src/reactive";
import {emit} from "./componentEmit";
import {initSlots} from "./componentSlot";
import {proxyRefs} from "../reactive";

// 保存当前的组件实例
let currentInstance: null | ComponentInstance = null;

export function createComponentInstance(vNode: VComponentNode, parent: InstanceParent): ComponentInstance {
    const components: ComponentInstance = {
        vNode,
        type: vNode.type,
        props: {},
        emit: () => {
        },
        provides: parent ? parent.provides : {},
        slots: {},
        parent,
        isMounted: false
    }
    components.emit = emit.bind(null, components)

    return components;
}


export function setupComponent(instance: ComponentInstance) {
    // TODO 实现以下
    initProps(instance);
    initSlots(instance)


    setupStateFulComponent(instance);
}

function finishComponentSetup(instance: ComponentInstance) {
    const component = instance.type;
    if (!isString(component) && component.render) {
        instance.render = component.render;
    }

}

function handleSetupResult(instance: ComponentInstance, setupResult: NormalObject) {
    // 处理两种情况
    // TODO function 情况


    if (isFunction(setupResult)) {

    } else if (isObject(setupResult)) {
        // 处理object 情况
        // 返回的数据 可能是ref 这个时候 需要自动解ref
        instance.setupState = proxyRefs(setupResult);
    }

    finishComponentSetup(instance);
}

function setupStateFulComponent(instance: ComponentInstance) {

    const component = instance.type;

    // ctx 在组件实例上挂载 代理
    instance.proxy = new Proxy({_: instance}, publicInstanceProxyHandle)
    const {setup} = component;
    if (setup) {
        // 为currentInstance赋值
        setCurrentInstance(instance)
        const setupResult = setup(shallowReadonly(instance.props as object), {
            emit: instance.emit
        });
        // 将currentInstance清空
        setCurrentInstance(null)
        handleSetupResult(instance, setupResult);
    }
}


/**
 * 获取当前组件实例
 */
export function getCurrentInstance(): typeof currentInstance {
    return currentInstance
}


/**
 * 设置当前组件实例
 */
function setCurrentInstance(instance: typeof currentInstance): void {
    currentInstance = instance
}