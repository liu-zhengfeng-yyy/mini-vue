import {ObjectKey} from "./type";
import {getCurrentInstance} from "./component";
import {isFunction} from "../shared/is";

export function inject(key: ObjectKey, defaultValue: any) {
    // 存 注入操作
    const currentInstance = getCurrentInstance();
    if (currentInstance?.parent?.provides?.[key]) {
        return currentInstance.parent.provides[key];
    } else if (defaultValue) {
        return isFunction(defaultValue) ? defaultValue() : defaultValue;
    }

}


export function provide(key: ObjectKey, value: any) {

    // 存 注入操作
    const currentInstance = getCurrentInstance();
    if (currentInstance) {
        let {provides} = currentInstance;
        const parentProvides = currentInstance.parent?.provides;

        if (provides && parentProvides && provides === parentProvides) {
            // 初始化的时候
            provides = currentInstance.provides = Object.create(parentProvides)
        }
        Reflect.set(provides, key, value)
    }
}