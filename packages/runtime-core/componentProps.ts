import {ComponentInstance} from "./type";

export function initProps(instance: ComponentInstance) {
    // 初始化 props
    instance.props = instance.vNode.props || {};
}