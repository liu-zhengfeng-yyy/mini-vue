import {VNode, VNodeChildren, VNodeProps, VNodeType} from "./type";
import {ShapeFlags} from "./shapeFlags";
import {isArray, isObject, isString} from "../shared/is";

export const FRAGMENT = Symbol('FRAGMENT');
export const TEXT = Symbol('TEXT');

export function createVNode(type: VNodeType, props: VNodeProps = {}, children?: VNodeChildren): VNode {

    const vNode: VNode = {
        type,
        props,
        children,
        el: null,
        shapeFlag: getShapeFlag(type),
        key: props && props.key,
        setup: () => ({})
    }
    // 需要处理children的ShapeFlag
    if (isString(children)) {
        // string
        vNode.shapeFlag |= ShapeFlags.TEXT_CHILDREN;
    } else if (isArray(children)) {
        // 数组
        vNode.shapeFlag |= ShapeFlags.ARR_CHILDREN;
    }

    // 如果是组件 并且children是object
    if (vNode.shapeFlag & ShapeFlags.STATEFUL_COMPONENT && isObject(vNode.children)) {
        vNode.shapeFlag |= ShapeFlags.SLOT_CHILDREN
    }
    return vNode;

}

/**
 * 获取当前节点的shape flag
 * @param type
 */
function getShapeFlag(type: VNodeType): ShapeFlags {
    return isString(type) ? ShapeFlags.ELEMENT : ShapeFlags.STATEFUL_COMPONENT;
}

/**
 * 创建纯文本节点
 * @param text
 */
export function createTextVNode(text: string) {
    return createVNode(TEXT, {}, text)
}