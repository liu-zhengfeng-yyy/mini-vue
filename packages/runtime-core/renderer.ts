import {createComponentInstance, setupComponent} from "./component";
import {isArray, isFunction, isObject} from "../shared/is";
import type {
    ComponentInstance,
    ObjectKey,
    VComponentNode,
    VElementNode,
    VNode,
    VNodeChildren,
    VNodeEl,
    VNodeProps
} from "./type";
import {InstanceParent, OldNode} from "./type";
import {ShapeFlags} from "./shapeFlags";
import {FRAGMENT, TEXT} from "./createVNode";
import {createAppApi} from "./createApp";
import {effect} from "../reactive/src/effect";


const EMPTY_OBJ = {};


export function createRender(options: Record<ObjectKey, any>) {

    const {
        createElement: hostCreateElement,
        patchProps: hostPatchProps,
        insert: hostInsert,
        remove: hostRemove,
        setElementText: hostSetElementText
    } = options;

    function render(initialVNode: VNode, container: HTMLElement, parent: InstanceParent = null) {
        console.log('---------render---------------');
        console.log('container', container)
        // 渲染时 先调用patch对比虚拟节点
        patch(null, initialVNode, container, parent, null);
    }


    function patch(oldVNode: OldNode, newVNode: VNode, container: HTMLElement, parent: InstanceParent, anchor: VNodeEl) {
        // 这个时候需要 判断vNode是不是一个element
        // 判断依据比较简单 就是 如果是一个element 应该是 'div' 。。。。 字符串
        // 如果是一个组件类型 那么应该是一个object
        // 这边也需要处理一下 Fragment -> 只渲染children
        const {type, shapeFlag} = newVNode;
        if (type === FRAGMENT) {
            // 处理Fragment类型
            processFragment(oldVNode, newVNode.children as VNode[], container, parent, anchor);
        } else if (type === TEXT) {
            // 处理Text类型
            processText(oldVNode, newVNode, container);
        } else {
            if (shapeFlag & ShapeFlags.ELEMENT) {
                // 处理element类型
                processElement(oldVNode, newVNode as VElementNode, container, parent, anchor);
            } else if (shapeFlag & ShapeFlags.STATEFUL_COMPONENT) {
                // component 类型
                processComponent(oldVNode, newVNode as VComponentNode, container, parent);
            }
        }


    }


    function patchProps(el: VNodeEl, oldProps: VNodeProps, newProps: VNodeProps) {
        if (oldProps !== newProps) {
            // 只有属性不一样时 才需要patch
            // 遍历新的属性
            for (let newPropsKey in newProps) {
                const newValue = newProps[newPropsKey];
                const oldValue = oldProps[newPropsKey];
                if (oldValue !== newValue) {
                    hostPatchProps(el, newPropsKey, oldValue, newValue)
                }

            }
            if (oldProps !== EMPTY_OBJ) {
                // 只有 oldProps 不为空 接下来的操作才有意义
                // 遍历旧的属性
                for (let oldPropsKey in oldProps) {
                    if (!(oldPropsKey in newProps)) {
                        // 旧属性 不在新属性里面 则需要删除
                        hostPatchProps(el, oldPropsKey, oldProps[oldPropsKey], null)
                    }
                }
            }

        }

    }

    function unmountChildren(children: VNodeChildren | undefined) {
        if (isArray(children)) {
            for (let child of children) {
                hostRemove(child.el)
            }
        }

    }

    function patchChildren(el: VNodeEl, oldVNode: VNode, newVNode: VElementNode, parent: InstanceParent, anchor: VNodeEl) {

        const {shapeFlag: oldShapeFlag, children: oldChildren} = oldVNode;

        const {shapeFlag: newShapeFlag, children: newChildren} = newVNode;
        if (newShapeFlag & ShapeFlags.TEXT_CHILDREN) {
            // 新节点是text节点
            if (oldShapeFlag & ShapeFlags.ARR_CHILDREN) {
                // 原先是数组children 现在是文本节点
                // 把之前的children node清空
                unmountChildren(oldChildren)
                // 把新的textNode 挂载
                hostSetElementText(el, newChildren)
            } else if (oldShapeFlag & ShapeFlags.TEXT_CHILDREN && newChildren !== oldChildren) {
                // 新节点和旧节点都是文本节点 并且文本不一样
                // 把新的textNode 挂载
                hostSetElementText(el, newChildren)
            }

            // 可优化为

            // if (oldShapeFlag & ShapeFlags.ARR_CHILDREN) {
            //     // 原先是数组children 现在是文本节点
            //     // 把之前的children node清空
            //     unmountChildren(oldChildren)
            // }

            // if (newChildren !== oldChildren) {
            //     // 把新的textNode 挂载
            //     hostSetElementText(el, newChildren)
            // }

        } else if (newShapeFlag & ShapeFlags.ARR_CHILDREN) {
            // 新的节点是数组
            if (oldShapeFlag & ShapeFlags.TEXT_CHILDREN) {
                // 之前是文本节点
                // 清空之前的文本节点
                hostSetElementText(el, '');
                // 挂载 新的数组节点
                mountChildren(newChildren as VNode[], el as HTMLElement, parent, anchor)
            } else {
                // 之前也是数组节点
                // 进行对比
                patchKeyChildren(oldChildren as Array<VNode>, newChildren as Array<VNode>, el as HTMLElement, parent, anchor)
            }
        }
    }


    /**
     * 对比key相同的子节点
     * @param oldChild
     * @param newChild
     */
    function isSameVNode(oldChild: VNode, newChild: VNode) {
        return oldChild && newChild && oldChild.key === newChild.key && oldChild.type === newChild.type
    }

    function patchKeyChildren(oldChildren: Array<VNode>, newChildren: Array<VNode>, container: HTMLElement, parentComponent: InstanceParent, anchor: VNodeEl) {

        let i = 0;
        let oldLength = oldChildren.length - 1;
        let newLength = newChildren.length - 1;

        // 从左侧开始对比
        while (i <= oldLength && i <= newLength) {
            // 索引值不能大于两个数组节点的最大索引值
            const oldChild = oldChildren[i];
            const newChild = newChildren[i];
            if (isSameVNode(oldChild, newChild)) {
                // 是同一个节点
                patch(oldChild, newChild, container, parentComponent, anchor)
            } else {
                // 不是同一个节点
                break
            }
            i++;
        }

        // 从右侧开始对比
        while (i <= oldLength && i <= newLength) {
            // 索引值不能大于两个数组节点的最大索引值
            const oldChild = oldChildren[oldLength];
            const newChild = newChildren[newLength];
            if (isSameVNode(oldChild, newChild)) {
                // 是同一个节点
                patch(oldChild, newChild, container, parentComponent, anchor)
            } else {
                // 不是同一个节点
                break
            }
            oldLength--;
            newLength--;
        }

        // 新的节点比旧的节点多 创建
        if (i > oldLength) {
            // i > oldLength 说明旧的节点已经遍历完了
            if (i <= newLength) {
                const nextPos = newLength + 1;
                const anchor = nextPos < newLength ? newChildren[nextPos].el : null;
                // 有新增的节点
                while (i <= newLength) {
                    patch(null, newChildren[i], container, parentComponent, anchor);
                    i++;
                }
            }
        } else if (i > newLength) {
            // 新的节点比旧的节点少 删除
            while (i <= oldLength) {
                // 有删除的节点
                hostRemove(oldChildren[i].el);
                i++;
            }
        } else {
            // 乱序对比 中间对比
            let sNew = i;
            let sOld = i;

            const toBePatched = newLength - sNew + 1;
            let patched = 0;
            const keyToNewIndexMap = new Map();


            const newIndexToOldIndexMap = new Array(toBePatched).fill(0);


            for (let i = sNew; i <= newLength; i++) {
                const child = newChildren[i];
                keyToNewIndexMap.set(child.key, i)
            }

            for (let i = sOld; i <= oldLength; i++) {
                const child = oldChildren[i];
                if (patched >= toBePatched) {
                    hostRemove(child.el)
                    break
                }


                let newIndex;
                if (child.key != null) {
                    // undefined null
                    newIndex = keyToNewIndexMap.get(child.key);
                } else {
                    // 没有key
                    for (let j = sNew; j <= newLength; j++) {
                        if (isSameVNode(child, newChildren[j])) {
                            newIndex = j;
                            break
                        }
                    }
                }
                if (newIndex == undefined) {
                    // 说明没有找到
                    hostRemove(child.el)
                } else {
                    newIndexToOldIndexMap[newIndex - sNew] = i + 1;
                    patch(child, newChildren[newIndex], container, parentComponent, null);
                    patched++;
                }
            }
            const increasingNewIndexSequence = getSequence(newIndexToOldIndexMap);

            let j = increasingNewIndexSequence.length - 1;
            // 从右到左遍历 倒叙遍历
            for (let i = toBePatched; i >= 0; i--) {
                const nextIndex = i + sNew;
                const nextChild = newChildren[nextIndex];
                const anchor = nextIndex + 1 < newChildren.length ? newChildren[nextIndex + 1].el : null;
                if(newIndexToOldIndexMap[i] === 0){
                    // 说明是新增的节点
                    patch(null, nextChild, container, parentComponent, anchor);
                }else {
                    if (j < 0 || i !== increasingNewIndexSequence[j]) {
                        // 说明这个节点需要移动
                        hostInsert(container, nextChild.el, anchor)
                    } else {
                        j--;
                    }
                }
            }

        }

    }

    /**
     * 获取最长递增子序列
     * @param arr
     */
    function getSequence(arr: number[]) {
        const p = arr.slice();
        const result = [0];
        let i, j, u, v, c;
        const len = arr.length;
        for (i = 0; i < len; i++) {
            const arrI = arr[i];
            if (arrI !== 0) {
                j = result[result.length - 1];
                if (arr[j] < arrI) {
                    p[i] = j;
                    result.push(i);
                    continue;
                }
                u = 0;
                v = result.length - 1;
                while (u < v) {
                    c = ((u + v) / 2) | 0;
                    if (arr[result[c]] < arrI) {
                        u = c + 1;
                    } else {
                        v = c;
                    }
                }
                if (arrI < arr[result[u]]) {
                    if (u > 0) {
                        p[i] = result[u - 1];
                    }
                    result[u] = i;
                }
            }
        }
        u = result.length;
        v = result[u - 1];
        while (u-- > 0) {
            result[u] = v;
            v = p[v];
        }
        return result;
    }

    function patchElement(oldVNode: VNode, initialVElement: VElementNode, parent: InstanceParent, anchor: VNodeEl) {
        const oldProps = oldVNode.props || EMPTY_OBJ;
        const newProps = initialVElement.props || EMPTY_OBJ;
        // 获取当前的dom节点
        // 并且将dom节点更新给 新node initialVElement
        const el = (initialVElement.el = oldVNode.el);
        // 对比子节点
        patchChildren(el, oldVNode, initialVElement, parent, anchor);
        // 对比属性
        patchProps(el, oldProps, newProps);
    }


    function processElement(oldVNode: OldNode, initialVElement: VElementNode, container: HTMLElement, parent: InstanceParent, anchor: VNodeEl) {
        // 挂载元素节点
        if (!oldVNode) {
            // 初始化
            mountElement(initialVElement, container, parent, anchor)
        } else {
            patchElement(oldVNode, initialVElement, parent, anchor)
        }
    }

    function processComponent(oldVNode: OldNode, newVNode: VComponentNode, container: HTMLElement, parent: InstanceParent) {
        mountComponent(newVNode, container, parent)
    }

    function processFragment(oldVNode: OldNode, newVNode: VNode[], container: HTMLElement, parent: InstanceParent, anchor: VNodeEl) {
        mountChildren(newVNode, container, parent, anchor)
    }

    function processText(oldVNode: OldNode, newVNode: VNode, container: HTMLElement) {
        // 创建文本节点 并进行挂载
        const element = newVNode.el = document.createTextNode(newVNode.children as string);
        container.append(element)
    }

    function setupRenderEffect(instance: ComponentInstance, container: HTMLElement, initialVNode: VComponentNode) {

        effect(() => {
            if (instance.render && isFunction(instance.render)) {
                if (!instance.isMounted) {
                    // 初始化
                    console.log('组件初始化')
                    const {proxy} = instance
                    // 这个 时候需要记住 subTree 在组件实例上
                    const subTree = (instance.subTree = instance.render.call(proxy));
                    patch(null, subTree, container, instance, null);
                    // 这里说明子树已经处理完成了 也就是mounted
                    // 这里的 vNode 是最初的vNode 也可以理解为组件配置对象
                    // subTree 是调用组件的render方法 生成的真实的虚拟节点
                    // 这里的每一个虚拟节点都会挂载一个el -> element
                    initialVNode.el = subTree.el

                    // 初始化结束
                    instance.isMounted = true
                } else {
                    // 初始化
                    console.log('组件更新')
                    const {proxy} = instance
                    const subTree = instance.render.call(proxy);
                    // 更新阶段 需要拿到之前的 subTree
                    const preSubTree = instance.subTree as VNode;

                    // 更新 instance.subTree
                    instance.subTree = subTree;

                    patch(preSubTree, subTree, container, instance, null);
                }


            }
        })
    }

    function mountComponent(initialVNode: VComponentNode, container: HTMLElement, parent: InstanceParent) {
        const instance = createComponentInstance(initialVNode, parent);
        setupComponent(instance);

        setupRenderEffect(instance, container, initialVNode);
    }


    /**
     * 挂载元素节点
     * @param initialVElement
     * @param container
     * @param parent
     * @param anchor
     */
    function mountElement(initialVElement: VElementNode, container: HTMLElement, parent: InstanceParent, anchor: VNodeEl) {

        // 创建元素
        // 并且将创建的元素存到虚拟节点上
        const element = (initialVElement.el = hostCreateElement(initialVElement.type));

        const {shapeFlag} = initialVElement;

        // 先分情况处理children
        const {children} = initialVElement;
        if (shapeFlag & ShapeFlags.TEXT_CHILDREN) {
            // 处理children 是字符串的情况
            element.textContent = children as string;
        } else if (shapeFlag & ShapeFlags.ARR_CHILDREN) {
            //  处理children是数组的情况
            mountChildren(children as VNode[], element, parent, anchor)
        }

        // 处理props
        const {props} = initialVElement;
        if (props && isObject(props)) {
            Object.keys(props).forEach(key => {
                const value = props[key]
                // 初始化 oldValue 为空
                hostPatchProps(element, key, null, value);
            })
        }

        // 将创建的元素插入指定容器
        // container.append(element)
        hostInsert(container, element, anchor)
    }

    /**
     * 挂载子元素数组情况
     * @param children
     * @param container
     * @param parent
     * @param anchor
     */
    function mountChildren(children: VNode[], container: HTMLElement, parent: InstanceParent, anchor: VNodeEl) {
        children.forEach(child => {
            // 拿到数组的每一项 调用patch
            patch(null, child, container, parent, anchor)
        })
    }

    return {createApp: createAppApi(render)};
}


