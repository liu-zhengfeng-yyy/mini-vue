// createApp mount() 中参数类型
import {ShapeFlags} from "./shapeFlags";

export type ContainerElement = string | HTMLElement

export type VNodeEmit = (...args: any[]) => void;

export interface VNodeContext {
    emit: VNodeEmit;
}

// VNode 虚拟节点类型
export interface VNode {
    type: VNodeType;
    props: VNodeProps;
    render?: () => VNode;
    setup: (props: VNodeProps, ctx: VNodeContext) => NormalObject;
    children?: VNodeChildren;
    el: VNodeEl,
    shapeFlag: ShapeFlags,
    key?: string | number,
}


export interface VElementNode extends VNode {
    type: string;
}

export interface VComponentNode extends VNode {
    type: VNode;
}


export type VNodeProps = NormalObject
export type VNodeType = string | Symbol | VNode
export type VNodeChildren = Array<VNode> | string | NormalObject;
export type VNodeEl = null | HTMLElement | Text;

export interface ComponentInstance {
    props: VNodeProps;
    type: VNode;
    vNode: VNode;
    render?: () => VNode;
    proxy?: Object;
    setupState?: NormalObject;
    emit: VNodeEmit;
    slots: NormalObject;
    provides: NormalObject,
    parent: InstanceParent,
    isMounted?: boolean,
    subTree?: VNode
}

export type InstanceParent = ComponentInstance | null;
export type ObjectKey = string | symbol;

export type NormalObject<T = any> = Record<ObjectKey, T>

export type OldNode = VNode | null