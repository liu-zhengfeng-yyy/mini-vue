import {createVNode} from './createVNode';
import {VNode, VNodeProps} from "./type";

export function h(type: VNode, props?: VNodeProps, children?: VNode[]): VNode {
    return createVNode(type, props, children);
}