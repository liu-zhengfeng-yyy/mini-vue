export enum ShapeFlags {
    ELEMENT = 1, // 元素节点
    STATEFUL_COMPONENT = 1 << 1, // 组件节点
    TEXT_CHILDREN = 1 << 2, // 文本节点
    ARR_CHILDREN = 1 << 3, // 数组
    SLOT_CHILDREN = 1 << 4, // 插槽节点
}