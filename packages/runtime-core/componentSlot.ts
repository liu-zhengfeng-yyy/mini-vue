import {ComponentInstance, NormalObject} from "./type";
import {isArray} from "../shared/is";
import {ShapeFlags} from "./shapeFlags";

export function initSlots(instance: ComponentInstance) {
    const children = instance.vNode.children;
    // 初始化 slots
    const {vNode} = instance;
    if (vNode.shapeFlag & ShapeFlags.SLOT_CHILDREN) {
        normalizeSlots(instance, children as NormalObject)
    }
}

function normalizeSlots(instance: ComponentInstance, children: NormalObject) {
    const slots: NormalObject = {};
    for (const key in children) {
        const value = children[key];
        slots[key] = (props: NormalObject) => normalizeSlotsValue(value(props))
    }
    instance.slots = slots
}

function normalizeSlotsValue(value: any) {
    return isArray(value) ? value : [value]
}