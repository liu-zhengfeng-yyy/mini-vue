import {ComponentInstance} from "./type";
import {isFunction} from "../shared/is";
import {handleEventName} from '../shared/utils'

export function emit(instance: ComponentInstance, event: string, ...args: any[]) {
    console.log('component emit', event, args)
    // 解析当前组件的props
    const {props} = instance;
    const eventName = handleEventName(event);
    const handle = props[eventName]
    if (isFunction(handle)) {
        handle(...args)
    }
}