import {createVNode, FRAGMENT} from "../createVNode";
import {NormalObject} from "../type";
import {isFunction} from "../../shared/is";

export function renderSlots(slots: NormalObject, key: string, props: NormalObject) {
    let slot = slots[key];
    if (slot) {
        if (isFunction(slot)) {
            slot = slot(props)
        }
        return createVNode(FRAGMENT, {}, slot)
    }
}