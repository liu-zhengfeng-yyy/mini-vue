import {createVNode} from "./createVNode";
import type {ContainerElement} from "./type";
import {isString} from "../shared/is";
import {VNode} from "./type";

export function createAppApi(render: (vNode: VNode, container: HTMLElement) => void) {
    return function createApp(rootComponent: VNode) {
        return {
            mount(rootContainer: ContainerElement) {
                // 兼容用户直接传选择器string情况
                let container: HTMLElement | null = null;
                if (isString(rootContainer)) {
                    container = document.querySelector(rootContainer);
                }
                if (container === null) {
                    throw new Error("root container is null")
                }
                // 先将所有的东西 转换成vNode  虚拟节点
                const vNode = createVNode(rootComponent);
                // 根据虚拟节点进行渲染
                render(vNode, container);
            }
        }
    }
}



