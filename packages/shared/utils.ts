/**
 * 将首字母大写
 * @param str
 */
export function capitalize(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * 处理事件命名
 * @param str
 */
export function handleEventName(str: string): string {
    return str ? `on${capitalize(str)}` : ''
}