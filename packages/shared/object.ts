export type ObjectKey = string | symbol;

/**
 * 判断key是不是 obj的属性
 * @param obj
 * @param key
 */
export function hasOwn(obj: any, key: ObjectKey): boolean {
    return obj && Object.prototype.hasOwnProperty.call(obj, key)
}