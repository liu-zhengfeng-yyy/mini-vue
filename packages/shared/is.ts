/**
 * 判断数据是不是object类型
 * @param data
 */
export function isObject(data: any): data is Object {
    return data !== null && typeof data == 'object'
}

/**
 * 判断数据是不是function类型
 * @param data
 */
export function isFunction(data: any): data is Function {
    return data && typeof data == 'function'
}

/**
 * 判断数据是不是string类型
 * @param data
 */
export function isString(data: any): data is string {
    return data && typeof data == 'string'
}

/**
 * 判断数据是不是Array类型
 * @param data
 */
export function isArray(data: any): data is Array<any> {
    return data && Array.isArray(data)
}

/**
 * 判断两个数是不是改变过
 * @param value1
 * @param value2
 */
export function isChange(value1: any, value2: any): boolean {
    return !Object.is(value1, value2)
}

/**
 * 判断字符串是不是on开头
 * @param value
 */
export function isOnStart(value: string): boolean {
    return value.startsWith('on')
}
