import {ReactiveEffect} from "../src/effect";

export type TargetKey = string | symbol;

export interface EffectOptions {
    scheduler?: Fun;
    onStop?: Fun
}

export type Fun = () => any;

export type Runner = (() => any) & { effect?: ReactiveEffect }

export type ComputedFun = () => any
