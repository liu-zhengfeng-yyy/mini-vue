import {ComputedFun} from "../types";
import {ReactiveEffect, triggerEffects, trackEffects} from "./effect";
import {createDeps} from "./dep";

class ComputedRefImpl {
    public getter: ComputedFun;
    private _value: any;
    private effect: ReactiveEffect;
    private _dirty: boolean;
    private dep: Set<ReactiveEffect>;

    constructor(getter: ComputedFun) {
        this._dirty = true;
        this.getter = getter;
        this.dep = createDeps()
        this.effect = new ReactiveEffect(getter, () => {
            // scheduler
            // 只要触发了这个函数说明依赖项更新了
            // 那么就解锁，后续在调用 get 的时候就会重新执行，所以会得到最新的值
            if (this._dirty) return;

            this._dirty = true;

            // 当数据发生变化的时候 需要派发更新
            triggerEffects(this.dep);
        })
    }


    get value() {
        // 收集依赖
        trackEffects(this.dep)
        // 这里主要做缓存如果 _dirty为false就说明依赖项没有变化
        if (this._dirty) {
            // 这里说明 是第一次触发get或者依赖数据已经发生了变化
            this._dirty = false;
            // 这里执行 _run 的话，就是执行用户传入的 fn
            // 获取最新的数据
            this._value = this.effect._run();
        }
        return this._value
    }

}

/**
 * 实现computed函数 执行fun执行依赖 获取一个数据
 * @param getter
 */
export function computed(getter: ComputedFun): any {
    return new ComputedRefImpl(getter)
}