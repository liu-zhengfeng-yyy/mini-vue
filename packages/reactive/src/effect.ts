import {EffectOptions, Fun, TargetKey, Runner} from "../types";
import {createDeps} from "./dep";


// 全局变量 保存当前正在执行（收集依赖）的函数
let activeEffect: ReactiveEffect | undefined;
// 当前依赖的Map
const depMaps: Map<object, Map<TargetKey, Set<ReactiveEffect>>> = new Map();
// 全局变量 是否应该收集依赖
let shouldTrack = false;

export class ReactiveEffect {
    // 需要执行的函数
    private readonly _fun: Fun;
    // 调用effect的时候 配置的scheduler
    public scheduler: Fun | undefined;
    public deps: Array<Set<ReactiveEffect>>;
    // 当前effect是不是激活状态
    public active = true;
    // onStop 当执行 stop 方法时回调
    public onStop: Fun | undefined;


    constructor(fun: Fun, scheduler?: Fun) {
        this._fun = fun
        this.scheduler = scheduler;
        // 初始化依赖项数组
        this.deps = [];
    }

    _run() {
        if (!this.active) {
            //   如果当前不是活跃的 就执行了一下fn 不收集依赖
            return this._fun();
        }
        // 打开依赖收集开关
        shouldTrack = true
        // 执行函数之前 先修改全局变量 以便于 依赖收集
        activeEffect = this;
        // 执行函数  返回执行结果
        const result = this._fun();
        // 执行完fn后 关闭开关
        shouldTrack = false
        //  此时将activeEffect置空
        activeEffect = undefined;
        return result;
    }

    _stop() {
        // 如果当前是激活状态 就清空依赖
        if (this.active) {
            //  清空依赖
            cleanupEffect(this);
            // 如果注册了这个回调 就执行这个回调
            if (this.onStop) {
                this.onStop()
            }
            // 将状态设置为false(失活状态)
            this.active = false;
        }
    }
}


/**
 * 清空当前effect的依赖项
 * @param effect
 */
function cleanupEffect(effect: ReactiveEffect) {
    effect.deps.forEach(dep => {
        // 将所有数据的依赖项中删除当前的effect
        dep.delete(effect)
    })
    effect.deps.length = 0;
}

/**
 *  收集依赖
 * @param target  当前的响应式数据
 * @param key    当前的key
 */
export function track(target: object, key: TargetKey) {
    // 如果当前全局没有activeEffect 就不收集依赖
    if (!activeEffect) return;
    // 如果shouldTrack为false也不收集依赖
    if (!shouldTrack) return;
    // 当前的target 作为key
    let depMap = depMaps.get(target);
    if (!depMap) {
        // 当前的target没有依赖
        depMap = new Map();
        depMaps.set(target, depMap);
    }
    // 根据key 拿到 key的所有依赖
    let dep = depMap.get(key);
    if (!dep) {
        //   当前的key 没有依赖
        dep = createDeps();
        depMap.set(key, dep)
    }
    trackEffects(dep);
}

/**
 * 收集依赖
 * @param dep
 */
export function trackEffects(dep: Set<ReactiveEffect>) {
    // 如果当前全局没有activeEffect 就不收集依赖
    if (!activeEffect) return;
    // 搜索全局的指定变量 如果有就收集当前依赖
    dep.add(activeEffect)
    //  将依赖项 收集到 activeEffect 指定字段中
    activeEffect.deps.push(dep);
}

/**
 * 触发依赖
 * @param target 当前的响应式数据
 * @param key 响应式数据的属性
 */
export function trigger(target: object, key: TargetKey) {
    const depMap = depMaps.get(target);
    if (!depMap) return;
    const deps = depMap.get(key);
    if (!deps) return;
    // 拿到了目前指定对象和键的依赖数据;
    triggerEffects(deps)
}


/**
 * 循环依赖 effect 派发更新
 * @param deps
 */
export function triggerEffects(deps:Set<ReactiveEffect>){
    for (const dep of deps) {
        // 执行每个依赖项
        // 这里需要判断一下 是不是配置了scheduler 如果配置了 那么更新的时候执行的是scheduler
        dep.scheduler ? dep.scheduler() : dep._run();
    }
}


// 执行一个函数 并且收集当前函数执行过程中的依赖
export function effect(fun: Fun, options?: EffectOptions): Runner {
    // 这里配置 effect的scheduler
    // 执行情况是 首次执行effect时 不会执行这个scheduler
    // 但是当依赖数据更新时 如果配置了scheduler就会执行scheduler 不会执行 fun
    const _effect = new ReactiveEffect(fun, options?.scheduler);
    _effect._run();

    // 如果配置了onStop 就注册onStop
    _effect.onStop = options?.onStop;

    // 执行 effect 返回一个 runner  这个runner调用之后会重新执行一次effect函数 并且返回fn的执行结果
    // 这里执行的时候 需要绑定this
    const runner: Runner = _effect._run.bind(_effect);
    // 为了通过runner能得到runner中的effect 所以在runner上将当前的_effect进行绑定
    runner.effect = _effect;
    return runner;

}


/**
 * 停止当前runner上的依赖执行 也就是清空当前effect的所有依赖
 * @param runner
 */
export function stop(runner: Runner) {
    runner.effect?._stop();
}



