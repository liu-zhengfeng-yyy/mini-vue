import {trackEffects, triggerEffects} from "./effect";
import {isChange, isObject} from "../../shared/is";
import {reactive} from "./reactive";
import {ObjectKey} from "../../shared/object";

class RefImpl {
    public _value: any
    public deps: Set<any>
    public _v_isRef = true
    private _rawValue: any

    constructor(value: any) {
        this._value = convert(value);
        this._rawValue = value
        this.deps = new Set();
    }

    get value() {
        // get 的时候 应该收集依赖
        trackEffects(this.deps)
        return this._value
    }

    set value(newValue) {
        // 如果value为object 则 this._value 为 reactive 此时对比是没有意义的
        // 使用_rawValue保存最原始的对象数据
        if (isChange(newValue, this._rawValue)) {
            // set的时候应该派发更新 但是要先设置数据
            Reflect.set(this, '_value', convert(newValue));
            Reflect.set(this, '_rawValue', newValue);
            triggerEffects(this.deps)
        }
    }

}

/**
 * 生成ref数据
 * @param value
 */
export function ref(value: any): RefImpl {
    return new RefImpl(value)
}

/***
 * 转换数据 如果是 object 就转换为 reactive
 * 如果不是 就返回原始数据
 * @param value
 */
export function convert(value: any) {
    return isObject(value) ? reactive(value) : value
}


/**
 * 判断一个数据是不是 ref
 * @param value
 */
export function isRef(value: any): boolean {
    return Boolean(value._v_isRef)
}

/**
 * 获取ref对象的value值
 * @param value
 */
export function unRef(value: any): any {
    return isRef(value) ? value.value : value
}


export function proxyRefs(objectWithRefs: Record<ObjectKey, any>) {
    return new Proxy(objectWithRefs, {
        get(target: object, key: ObjectKey): any {
            return unRef(Reflect.get(target, key))
        },
        set(target: Record<ObjectKey, any>, key: string | symbol, newValue: any): boolean {
            if (isRef(target[key]) && !isRef(newValue)) {
                return (target[key].value = newValue)
            } else {
                return Reflect.set(target, key, newValue)
            }
        }
    })
}
