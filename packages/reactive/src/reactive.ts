import {
    createActiveObject,
    mutableHandles,
    reactiveFlag,
    readonlyHandles,
    shallowReactiveHandles,
    shallowReadonlyHandles
} from './baseHandlers'

/**
 * reactive 函数 创建响应式对象
 * @param data
 */
export function reactive(data: object): any {
    return createActiveObject(data, mutableHandles)
}

/**
 * readonly 函数 创建readonly响应式对象
 * @param data
 */
export function readonly(data: object): any {
    return createActiveObject(data, readonlyHandles)
}

/**
 * shallowReactive 函数 创建浅层响应式对象
 * @param data
 */
export function shallowReactive(data: object): any {
    return createActiveObject(data, shallowReactiveHandles)
}

/**
 * shallowReadonly 函数 创建浅层只读响应式对象
 * @param data
 */
export function shallowReadonly(data: object): any {
    return createActiveObject(data, shallowReadonlyHandles)
}

/**
 * 判断一个数据是不是reactive
 * @param data
 */
export function isReactive(data: any): boolean {
    return !!data[reactiveFlag.REACTIVE]
}

/**
 * 判断一个数据是不是readonly
 * @param data
 */
export function isReadonly(data: any): boolean {
    return !!data[reactiveFlag.READONLY]
}

/**
 * 判断一个数据是不是proxy
 * 就是判断数据是reactive或者readonly
 * @param data
 */
export function isProxy(data: any): boolean {
    return isReadonly(data) || isReactive(data)
}
