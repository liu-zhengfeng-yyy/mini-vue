import {TargetKey} from "../types";
import {track, trigger} from "./effect";
import {isObject} from "../../shared/is";
import {reactive, readonly} from "./reactive";

// 响应式数据的flag
export enum reactiveFlag {
    REACTIVE = '__v__isReactive',
    READONLY = '__v__isReadonly'
}

/**
 * getter 创建函数
 * @param isReadonly  是不是只读
 * @param isShallow 是不是浅层的
 */
export function createGetter(isReadonly = false, isShallow = false) {
    return (target: object, p: TargetKey, receiver: any): any => {
        // 这里是要判断当前数据是不是reactive
        if (p === reactiveFlag.REACTIVE) {
            // 只要不是readonly就是reactive
            return !isReadonly
        }
        //  这里是要判断当前数据是不是readonly
        if (p === reactiveFlag.READONLY) {
            // 只要不是readonly就是reactive
            return isReadonly
        }

        // 先得到结果
        const result = Reflect.get(target, p, receiver);

        // 不是readonly 再收集依赖
        if (!isReadonly) {
            track(target, p)
        }

        if (isShallow) {
            // 如果shallow为true 则证明只需要浅层的响应式
            return result
        }
        //判断当前的result是不是嵌套object
        if (isObject(result)) {
            // 嵌套对象 需要嵌套处理
            return isReadonly ? readonly(result) : reactive(result)
        }
        // 返回结果
        return result;
    }
}


/**
 * setter 创建函数
 * @param isReadonly
 */
export function createSetter(isReadonly = false) {
    return (target: object, p: TargetKey, value: any, receiver: any): boolean => {
        if (isReadonly) {
            console.warn('不能给readonly数据设置值');
            // return false 代表 set过程中出现了错误
            return true;
        }
        // 先设置对应的数据
        const result = Reflect.set(target, p, value, receiver)
        // 触发依赖
        trigger(target, p)
        // 返回设置的结果
        return result;
    }
}

/**
 * reactive 的 get
 */
const get = createGetter();
/**
 * reactive 的 set
 */
const set = createSetter();

/**
 * readonly 的 get
 */
const readonlyGet = createGetter(true);
/**
 * readonly 的 set
 */
const readonlySet = createSetter(true);
/**
 * shallowReadonly 的 get
 */
const shallowReadonlyGet = createGetter(true, true);
/**
 * shallowReadonly 的 set
 */
const shallowReadonlySet = createSetter();

/**
 * shallowReactive 的 get
 */
const shallowReactiveGet = createGetter(false, true);
/**
 * shallowReactive 的 set
 */
const shallowReactiveSet = createSetter();

/**
 * reactive 的 proxy handle
 */
export const mutableHandles = {
    get,
    set
}


/**
 * readonly 的 proxy handle
 */
export const readonlyHandles = {
    get: readonlyGet,
    set: readonlySet
}

/**
 * shallowReadonly 的 proxy handle
 */
export const shallowReadonlyHandles = {
    get: shallowReadonlyGet,
    set: shallowReadonlySet
}

/**
 * shallowReactiveHandles 的 proxy handle
 */
export const shallowReactiveHandles = {
    get: shallowReactiveGet,
    set: shallowReactiveSet
}


/**
 * 创建一个proxy对象
 * @param data 原始数据
 * @param handle proxy handle
 */
export function createActiveObject(data: object, handle: ProxyHandler<object>): any {
    if (!isObject(data)) {
        console.warn(`target : ${data} must be a object`);
        return
    }
    return new Proxy(data, handle)
}