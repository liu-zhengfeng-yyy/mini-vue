import {reactive, readonly, isReactive, isReadonly, isProxy, shallowReactive, shallowReadonly} from "../src/reactive";
import {expect} from "vitest";

describe('响应式代理对象', () => {
    test('reactive函数 调用生成新的对象', () => {
        const origin = {foo: 1};
        const proxy = reactive(origin);
        expect(origin).not.toBe(proxy);
        expect(origin.foo).toBe(proxy.foo)
    })
    test('isReactive 判断数据是不是reactive', () => {
        const origin = {foo: 1};
        const proxy = reactive(origin);
        const readonlyProxy = readonly(origin)
        expect(isReactive(proxy)).toBe(true);
        expect(isReactive(origin)).toBe(false);
        expect(isReactive(readonlyProxy)).toBe(false);

    })
    test('isReactive 判断数据是不是readonly', () => {
        const origin = {foo: 1};
        const readonlyProxy = readonly(origin);
        const proxy = reactive(origin)
        expect(isReadonly(readonlyProxy)).toBe(true);
        expect(isReadonly(origin)).toBe(false);
        expect(isReadonly(proxy)).toBe(false);
    })
    test('isProxy 判断数据是不是Proxy', () => {
        const origin = {foo: 1};
        const readonlyProxy = readonly(origin);
        const proxy = reactive(origin)
        expect(isProxy(readonlyProxy)).toBe(true);
        expect(isProxy(origin)).toBe(false);
        expect(isProxy(proxy)).toBe(true);
    })
    test('响应式对象嵌套情况', () => {
        const origin = {
            foo: {
                data: 1
            }
        };
        //  制度代理对象
        const readonlyProxy = readonly(origin);
        // reactive代理对象
        const proxy = reactive(origin)
        expect(isReactive(proxy.foo)).toBe(true);
        expect(isReactive(readonlyProxy.foo)).toBe(false);
        expect(isReactive(origin.foo)).toBe(false);
        expect(isReadonly(readonlyProxy.foo)).toBe(true);
        expect(isReadonly(readonlyProxy.foo)).toBe(true);
        expect(isReadonly(proxy.foo)).toBe(false);

        // 模拟console.warn函数
        console.warn = vi.fn();
        // 浅层代理对象
        const shallowProxy = shallowReactive(origin);
        // 浅层只读代理对象
        const shallowReadonlyProxy = shallowReadonly(origin);
        expect(isReactive(shallowProxy.foo)).toBe(false);
        expect(isReadonly(shallowReadonlyProxy.foo)).toBe(false);
        // 给readonly对象赋值会警告
        readonlyProxy.foo.data = 2;
        expect(console.warn).toBeCalledTimes(1);
        // 浅层的readonly对象赋值不会警告
        shallowReadonlyProxy.foo.data = 2;
        expect(console.warn).toBeCalledTimes(1);
    })
})