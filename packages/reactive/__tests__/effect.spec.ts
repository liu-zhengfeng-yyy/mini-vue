import {effect,stop} from "../src/effect";
import {reactive} from "../src/reactive";

describe('effect 函数', () => {
    test('effect 函数 收集依赖派发更新', () => {
        // 原始对象
        const origin = {num: 10};
        // 依赖origin的目标数据
        let targetNum = 0;
        // 响应式数据
        const reactiveOrigin = reactive(origin);

        //  用effect 执行函数 收集依赖
        effect(() => {
            targetNum = reactiveOrigin.num;
        })

        origin.num = 12;
        // 这个时候 应该还是effect执行后的结果
        expect(targetNum).toBe(10)

        reactiveOrigin.num = 12;
        //   这个时候会进行依赖派发
        expect(targetNum).toBe(12)

    });
    test('effect执行返回runner  执行runner得到fn返回结果', () => {
        let num = 10;
        const runner = effect(() => {
            return ++num;
        })
        // 这里 num 已经执行了一次
        expect(num).toBe(11);
        const result = runner();
        //  runner 执行 会导致 fn执行一次
        expect(num).toBe(12)
        // 这个result就是fn返回的结果
        expect(result).toBe(12)
    });
    test("scheduler", () => {
        let dummy;
        let run: any;
        const scheduler = vi.fn(() => {
            run = runner;
        });
        const obj = reactive({ foo: 1 });
        const runner = effect(
            () => {
                dummy = obj.foo;
            },
            { scheduler }
        );
        // 首次执行 不会执行 scheduler
        expect(scheduler).not.toHaveBeenCalled();
        expect(dummy).toBe(1);
        // 当依赖项更新的时候 会执行scheduler 不会执行fn
        obj.foo++;
        expect(scheduler).toHaveBeenCalledTimes(1);
        expect(dummy).toBe(1);
        // 执行 runner 就会执行传入的fn 依赖项更新
        run();
        // 这时 依赖数据更新
        expect(dummy).toBe(2);
    });
    test("stop", () => {
        let dummy;
        // 生成响应式数据
        const obj = reactive({ prop: 1 });
        // 得到 effect 返回 runner
        const runner = effect(() => {
            dummy = obj.prop;
        });
        // 重新设置响应式数据
        obj.prop = 2;
        // 这个时候会触发effect依赖项更新
        expect(dummy).toBe(2);
        // 这个时候执行stop方法 清空当前effect的所有依赖项
        stop(runner);
        // 当数据更新的时候 effect中的依赖数据不会跟新
        // obj.prop = 3;
        obj.prop++
        expect(dummy).toBe(2);

        // 执行stop后重新执行runner 会重新执行fn 但是不会再收集依赖
        runner();
        // 这个时候fn执行了导致数据更新
        expect(dummy).toBe(3);
        // 当数据再更新的时候 依赖数据不会更新
        obj.prop++
        expect(dummy).toBe(3);
    });
    test("onStop", () => {
        const onStop = vi.fn();
        const runner = effect(() => {}, {
            onStop,
        });

        stop(runner);
        expect(onStop).toHaveBeenCalled();
    });
})