import {readonly} from "../src/reactive";

describe('readonly函数 代理对象', () => {
    test('readonly函数 调用生成新的对象', () => {
        console.warn = vi.fn();
        const origin = {foo: 1};
        const proxy = readonly(origin);
        // 原始对象和代理对象不同
        expect(origin).not.toBe(proxy);
        // 代理对象的属性和原始对象相同
        expect(proxy.foo).toBe(1)
        // 代理对象的值不可以设置
        proxy.foo++;
        expect(console.warn).toHaveBeenCalled();
    })
})