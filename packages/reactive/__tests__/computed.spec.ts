import {computed} from "../src/computed";
import {reactive} from "../src/reactive";

describe('computed ref 数据', () => {

    test('访问value属性', () => {
        const com = computed(() => {
            return 123
        })
        expect(com.value).toBe(123)
    })
    test('访问value属性之前 fun 不会被调用', () => {
            const fun = vi.fn()
            const com = computed(fun);
            expect(fun).not.toBeCalled();
            com.value;
            expect(fun).toBeCalled();
        }
    )
    test('computed 响应式',()=>{
        const value = reactive({
            foo: 1,
        });
        const getter = vi.fn(() => {
            return value.foo;
        });
        const cValue = computed(getter);

        // 依赖项不变的时候 getter不会被再次调用
        cValue.value;
        expect(getter).toHaveBeenCalledTimes(1);

        // 依赖项发生变化 但是不触发 value getter 也不会调用 getter
        value.foo = 2;
        expect(getter).toHaveBeenCalledTimes(1);

        // 只有读取值的时候 才会调用 getter
        expect(cValue.value).toBe(2);
        expect(getter).toHaveBeenCalledTimes(2);


        cValue.value;
        expect(getter).toHaveBeenCalledTimes(2);
    })
})