import {ref,isRef,unRef} from '../src/ref'
import {effect} from "../src/effect";

describe('ref 响应式数据', () => {
    test('访问value属性', () => {
        const refValue = ref(1);
        expect(refValue.value).toBe(1)
    })
    test('ref is reactive', () => {
        const refValue = ref(1);
        let b = 0;
        effect(() => {
            b = refValue.value + 1;
        })
        expect(b).toBe(2);
        refValue.value = 3;
        expect(refValue.value).toBe(3);
        expect(b).toBe(4);
    })

    test('ref value is object', () => {
        const refValue = ref({name:1});
        let b = 0;
        effect(() => {
            b = refValue.value.name + 1;
        })
        expect(b).toBe(2);
        refValue.value.name = 3;
        expect(refValue.value.name).toBe(3);
        expect(b).toBe(4);
    })

    test('ref value is set but not change', () => {
        const refValue = ref(1);
        let b = 0;
        effect(() => {
            b = refValue.value + 1;
        })
        expect(b).toBe(2);
        refValue.value = 1;
        expect(refValue.value).toBe(1);
        expect(b).toBe(2);
    })

    test('isRef and unRef',()=>{
        const a = 1;
        const b = ref(1);
        expect(isRef(a)).toBe(false);
        expect(isRef(b)).toBe(true)
        expect(unRef(a)).toBe(1);
        expect(unRef(b)).toBe(1);
    })
})