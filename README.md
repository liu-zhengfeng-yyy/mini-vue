# mini-vue

> 手写vue3核心功能 了解学习vue3的原理

## Feature

### Reactive API

- [x] reactive 函数 生成响应式数据 收集依赖并触发依赖更新  <br/>
- [x] effect 函数 执行指定函数 帮助reactive收集依赖 <br/>
- [x] effect 会返回 runner方法, 调用runner方法会重新执行fn，并且得到fn的执行结果 <br/>
- [x] stop方法清空effect依赖 <br/>
- [x] effect支持配置onStop回调函数 <br/>
- [x] readonly 函数创建一个只读的代理对象 <br/>
- [x] isProxy,isReactive,isReadonly 判断响应式数据类型 <br/>
- [x] reactive 和 readonly 嵌套object处理 <br/>
- [x] shallowReadonly 得到浅层的只读代理对象  <br/>
- [x] shallowReactive 得到浅层的响应式代理对象  <br/>
- [x] ref 功能  <br/>
- [x] isRef,unRef 工具函数 <br/>
- [x] computed 功能 <br/>

### Runtime Core

- [x] 实现初始化 element 主流程 <br/>
- [x] 实现组件代理对象 ($el、this) <br/>
- [x] 使用shapeFlag判断节点类型 <br/>
- [x] 处理元素的事件监听 <br/>
- [x] 实现组件的props逻辑 <br/>
- [x] 实现组件的emit逻辑 <br/>
- [x] 实现组件的slots逻辑 <br/>
- [x] 实现Fragment和Text节点渲染 <br/>
- [x] 实现getCurrentInstance方法 <br/>
- [x] 实现provide、inject方法 <br/>
- [x] 实现自定义渲染createRender方法 <br/>
- [x] 实现组件更新的基本流程 <br/>
- [x] 实现更新element的props
