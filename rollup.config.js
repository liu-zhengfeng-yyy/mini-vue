import typescript from '@rollup/plugin-typescript';
import {defineConfig} from 'rollup'
import pkg from './package.json' assert {type: "json"};

const config = defineConfig({
    input: './packages/index.ts',
    output: [
        {
            // cjs
            format: 'cjs',
            file: pkg.main
        },
        {
            // esm
            format: 'es',
            file: pkg.module
        }
    ],
    plugins: [typescript({
        sourceMap: false
    })],

})


export default config;